<?php
namespace common\helpers;

use Yii;
use common\components\ActiveRecord;
use yii\web\UploadedFile;

class FileUpload extends ActiveRecord{
    /**
     * @param $attribute
     * @return null|string
     */
    public function getFilePath($attribute)
    {
        return $this->$attribute ? Yii::getAlias('@uploads') . '/' . $this->$attribute : null;
    }

    /**
     * @param $attribute
     * @return string
     */
    public function getFileUrl($attribute)
    {
        // return a default image placeholder if your source avatar is not found
        $file = $this->$attribute ?: 'default_user.jpg';
        return Yii::getAlias('@uploadsUrl') . '/' . $file;
    }


    /**
     * @param $attribute
     * @return bool|UploadedFile
     */
    public function loadFile($attribute)
    {
        // get the uploaded file instance. for multiple file uploads
        // the following data will return an array (you may need to use
        // getInstances method)
        $file = UploadedFile::getInstance($this, $attribute);

        // if no image was uploaded abort the upload
        if (!$file) {
            return false;
        }

        // generate a unique file name
        $file->name = Yii::$app->security->generateRandomString() . '.' . $file->extension;
        $this->$attribute = $file;

        // the uploaded image instance
        return $file;
    }

    /**
     * @param $attribute
     * @return bool
     */
    public function deleteFile($attribute)
    {
        $file = $this->getFilePath($attribute);
        if ($file !== null && file_exists($file)) {
            unlink($file) ? $this->$attribute = null : null;
            return true;
        } else {
            return false;
        }
    }
}