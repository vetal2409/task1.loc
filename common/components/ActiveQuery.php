<?php
namespace common\components;

class ActiveQuery extends \yii\db\ActiveQuery
{
    public function init()
    {
        parent::init();
    }


    public function resetScope()
    {
        $this->params([]);
        return $this;
    }

    /**
     * @return $this
     */
//    public function deleted()
//    {
//        $this->byDelete(ActiveRecord::STATUS_DELETED);
//        return $this;
//    }

    /**
     * @return $this
     */
    public function notDeleted()
    {
        $this->byDelete(ActiveRecord::STATUS_NOT_DELETED);
        return $this;
    }

    /**
     * @param integer $status
     * @return $this
     */
    public function byDelete($status)
    {
        $this->andWhere(['deleted' => $status]);
        return $this;
    }

}