<?php
namespace common\components;

use Yii;
use common\models\Role;
use yii\web\User;

/**
 * Class WebUser
 * @property bool role
 * @property bool roleId
 * @property bool isRootOrAdmin
 * @property bool isRoot
 * @property bool isAdmin
 * @property bool isManager
 * @property bool isPlayer
 */
class WebUser extends User
{
    const SESSION_ROLE_NAME = 'user.role';
    const SESSION_ROLE_ID = 'user.role_id';
    
    /**
     * @return bool
     */
    public function getIsAdmin()
    {
        return self::getRole() === Role::NAME_ADMIN;
    }

    /**
     * @return bool
     */
    public function getIsModer()
    {
        return self::getRole() === Role::NAME_MODER;
    }

    /**
     * @param array|int $roleId
     * @return bool
     */
    public function checkByRoleId($roleId)
    {
        if (is_array($roleId)) {
            return in_array(self::getRoleId(), $roleId, true);
        } else {
            return self::getRoleId() === $roleId;
        }
    }


    /**
     * @param \common\models\User $identity
     * @param bool $cookieBased
     * @param int $duration
     */
    protected function afterLogin($identity, $cookieBased, $duration)
    {
        parent::afterLogin($identity, $cookieBased, $duration);

        if(!Yii::$app->authManager->getAssignments($identity->id)){
            $role = Yii::$app->authManager->getRole( $identity->role->name );
            Yii::$app->authManager->assign($role, $identity->id);
        }

        Yii::$app->session->set(self::SESSION_ROLE_NAME, $identity->role->name);
        Yii::$app->session->set(self::SESSION_ROLE_ID, $identity->role_id);
    }

    protected function afterLogout($identity)
    {
        parent::afterLogout($identity);
        Yii::$app->session->remove(self::SESSION_ROLE_NAME);
        Yii::$app->session->remove(self::SESSION_ROLE_ID);
    }


    /**
     * @return string
     */
    public function getRole()
    {
        return Yii::$app->session->get(self::SESSION_ROLE_NAME);
    }

    /**
     * @return int
     */
    public function getRoleId()
    {
        return Yii::$app->session->get(self::SESSION_ROLE_ID);
    }

}