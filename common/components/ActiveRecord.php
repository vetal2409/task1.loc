<?php

namespace common\components;


/**
 * Class ActiveRecord
 * @package common\components
 * @property integer $created_at
 * @property integer $created_by
 * @property integer $updated_at
 * @property integer $updated_by
 * @property integer $deleted
 */
class ActiveRecord extends \yii\db\ActiveRecord
{
    const STATUS_DELETED = 1;
    const STATUS_NOT_DELETED = 0;

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            LogAttributeBehavior::className()
        ];
    }

}