<?php

namespace common\models;

use common\components\ActiveRecord;
use Yii;

/**
 * This is the model class for table "role".
 *
 * @property integer $id
 * @property string $name
 */
class Role extends ActiveRecord
{
    const NAME_ADMIN = 'Admin';
    const NAME_MODER = 'Manager';

    const ID_ADMIN = 1;
    const ID_MODER = 2;

    private static $rolesArray = [
        self::ID_ADMIN => self::NAME_ADMIN,
        self::ID_MODER => self::NAME_MODER
    ];


    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'role';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['name'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Role'
        ];
    }


    public static function getRoleName($roleId)
    {
        return self::$rolesArray[$roleId];
    }
}
