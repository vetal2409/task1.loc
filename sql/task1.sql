/*
Navicat MySQL Data Transfer

Source Server         : 127.0.0.1
Source Server Version : 50541
Source Host           : 127.0.0.1:3306
Source Database       : task1

Target Server Type    : MYSQL
Target Server Version : 50541
File Encoding         : 65001

Date: 2015-02-02 02:21:07
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for category
-- ----------------------------
DROP TABLE IF EXISTS `category`;
CREATE TABLE `category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) DEFAULT NULL,
  `created_at` int(11) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of category
-- ----------------------------
INSERT INTO `category` VALUES ('1', 'science', '1422654525', '2', null, null, '0');
INSERT INTO `category` VALUES ('2', 'medicine', '1422654870', '1', null, null, '0');
INSERT INTO `category` VALUES ('3', 'education', '1422654883', '1', null, null, '0');
INSERT INTO `category` VALUES ('4', 'politics', '1422654899', '1', null, null, '0');

-- ----------------------------
-- Table structure for migration
-- ----------------------------
DROP TABLE IF EXISTS `migration`;
CREATE TABLE `migration` (
  `version` varchar(180) NOT NULL,
  `apply_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`version`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of migration
-- ----------------------------
INSERT INTO `migration` VALUES ('m000000_000000_base', '1422521506');
INSERT INTO `migration` VALUES ('m130524_201442_init', '1422521510');
INSERT INTO `migration` VALUES ('m150128_154618_add_log_columns', '1422571301');
INSERT INTO `migration` VALUES ('m150128_154619_add_log_columns2', '1422654261');

-- ----------------------------
-- Table structure for news
-- ----------------------------
DROP TABLE IF EXISTS `news`;
CREATE TABLE `news` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `category_id` int(11) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `image` varchar(255) DEFAULT NULL,
  `body` text,
  `created_at` int(11) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of news
-- ----------------------------
INSERT INTO `news` VALUES ('1', '1', 'First news', 'knlNAlpDl-Zk5JNY-tQYZcY1f3GUbjwJ.jpg', '<p>some test</p>', '1422836313', '1', null, null, '0');
INSERT INTO `news` VALUES ('2', '4', 'Second news', 'AIpJcWvMSplDqTQa6eI5G-F9SWaNxOgx.jpg', '<ol><li>some second text</li><li>sdfs</li></ol>', '1422836344', '1', '1422836357', '1', '0');
INSERT INTO `news` VALUES ('3', '2', 'dsfg', null, '<p>dfsgdsfg</p>', '1422836371', '1', '1422836376', '1', '1');

-- ----------------------------
-- Table structure for role
-- ----------------------------
DROP TABLE IF EXISTS `role`;
CREATE TABLE `role` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of role
-- ----------------------------
INSERT INTO `role` VALUES ('1', 'Admin');
INSERT INTO `role` VALUES ('2', 'Moder');

-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `auth_key` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `password_hash` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password_reset_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `role_id` smallint(6) NOT NULL DEFAULT '4',
  `status` smallint(6) NOT NULL DEFAULT '10',
  `created_at` int(11) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of user
-- ----------------------------
INSERT INTO `user` VALUES ('1', 'admin', 'jEHkVuP02pK6QGl7_zB2nB6ScZh2UtJo', '$2y$13$RjrQ4ADFU5cppcJwDS8zfOIykpVnufS4vV3Q2sKDs8xt3J1V4sXym', null, 'vetal.sydo@gmal.com', '1', '10', '1422611171', '0', null, null, '0');
INSERT INTO `user` VALUES ('2', 'moder', '3n6IbBc5yA_sjh1BfOdaC6FcgYhkiusC', '$2y$13$jhNYYh93uMVbQs4djl8EZ.k65Qq2IjKYnV2g1KRXRZ37knnbJEtkq', null, 'vetal16k@inbox.ru', '2', '10', '1422612859', '0', null, null, '0');
