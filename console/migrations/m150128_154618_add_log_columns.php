<?php

use yii\db\Schema;
use yii\db\Migration;

class m150128_154618_add_log_columns extends Migration
{
    public function up()
    {
        $this->addColumns(\common\models\User::tableName());
    }

    public function down()
    {
        $this->dropColumns(\common\models\User::tableName());

        return true;
    }


    private function addColumns($tableName)
    {
        $this->addColumn($tableName, 'created_at', 'int');
        $this->addColumn($tableName, 'created_by', 'int');
        $this->addColumn($tableName, 'updated_at', 'int');
        $this->addColumn($tableName, 'updated_by', 'int');
        $this->addColumn($tableName, 'deleted', 'tinyint(1) default 0');
    }

    private function dropColumns($tableName)
    {
        $this->dropColumn($tableName, 'created_at');
        $this->dropColumn($tableName, 'created_by');
        $this->dropColumn($tableName, 'updated_at');
        $this->dropColumn($tableName, 'updated_by');
        $this->dropColumn($tableName, 'deleted');
    }
}
