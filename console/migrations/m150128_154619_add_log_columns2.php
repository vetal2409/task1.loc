<?php

use yii\db\Migration;

class m150128_154619_add_log_columns2 extends Migration
{
    public function up()
    {
        $this->addColumns(\common\models\Category::tableName());
        $this->addColumns(\common\models\News::tableName());
    }

    public function down()
    {
        $this->dropColumns(\common\models\Category::tableName());
        $this->dropColumns(\common\models\News::tableName());
        return true;
    }


    private function addColumns($tableName)
    {
        $this->addColumn($tableName, 'created_at', 'int');
        $this->addColumn($tableName, 'created_by', 'int');
        $this->addColumn($tableName, 'updated_at', 'int');
        $this->addColumn($tableName, 'updated_by', 'int');
        $this->addColumn($tableName, 'deleted', 'tinyint(1) default 0 not null');
    }

    private function dropColumns($tableName)
    {
        $this->dropColumn($tableName, 'created_at');
        $this->dropColumn($tableName, 'created_by');
        $this->dropColumn($tableName, 'updated_at');
        $this->dropColumn($tableName, 'updated_by');
        $this->dropColumn($tableName, 'deleted');
    }
}
